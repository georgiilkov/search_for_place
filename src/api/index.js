import axios from "axios";

const API_URL = 'https://api.foursquare.com/v2/venues/explore';
const CLIENT_ID = 'SXDQ2SECMY2PI00VFNOB3FWC5VJE2K32W3X0JEZTJBAQZTHI';
const CLIENT_SECRET = 'JFOYD3RUA0ZIVEEZ1GO3LAGADDLDAVM5GAUATSTE2WJ4LSQX';
const VERSION = '20181001';
const LIMIT = 5;

export const getNearbyPlaces = searchPlace => {
    return axios.get(API_URL, {
        params: {
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            near: searchPlace,
            v: VERSION,
            limit: LIMIT
        }
    }).then(response => {
        return response.data.response.groups[0].items;
    })
};