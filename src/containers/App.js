import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import SearchForm from '../components/SearchForm';
import Places from '../components/Places';
import Spinner from '../components/Spinner';
import Error from  '../components/Error';
import '../style/App.css';

class App extends Component {

    static propTypes = {
        searchedPlace: PropTypes.string,
        inputError: PropTypes.string,
        apiError: PropTypes.string,
        isFetching: PropTypes.bool
    }

    resultsTitle() {
        if (!this.props.isFetching &&!this.props.inputError && !this.props.apiError && this.props.searchedPlace && this.props.searchedPlace.length > 0) {
            return (<h4>Results for {this.props.searchedPlace}</h4>);
        }
    }

    render() {
        return (
            <div className="mainWrapper center-block">
                <h3>Search for a place</h3>
                <SearchForm/>
                <div>
                    {this.resultsTitle()}
                    <Places/>
                    <Spinner/>
                    <Error/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { searchedPlace, inputError, apiError, isFetching } = state
    return { searchedPlace, inputError, apiError, isFetching};
}

export default connect(mapStateToProps)(App)