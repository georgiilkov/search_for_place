import {getNearbyPlaces} from '../api';

export const REQUEST_PLACES = 'REQUEST_PLACES';
export const RECEIVE_PLACES = 'RECEIVE_PLACES';
export const API_ERROR = 'API_ERROR';
export const INPUT_ERROR = "INPUT_ERROR";
const SEARCH_WORD_MIN_LENGTH = 2;
const SEARCH_WORD_LENGTH_ERROR = `Search text should be at least ${SEARCH_WORD_MIN_LENGTH} characters long`;

export const searchPlaces = searchPlace => ({
    type: REQUEST_PLACES,
    searchPlace: searchPlace
});

export const receivePlaces = (places) => ({
    type: RECEIVE_PLACES,
    places: places.map(place => place.venue),
});

export const apiError = (searchPlace, error) => {
    return {
        type: API_ERROR,
        searchPlace: searchPlace,
        error
    }
};

export const inputError = () => {
    return {
        type: INPUT_ERROR,
        error: SEARCH_WORD_LENGTH_ERROR
    }
}

export const fetchPlaces = searchPlace => (dispatch, getState) => {
    if (!getState().isFetching) {
        if (searchPlace.length < SEARCH_WORD_MIN_LENGTH) {
            dispatch(inputError());
        } else {
            dispatch(searchPlaces(searchPlace));
            getNearbyPlaces(searchPlace)
                .then(places => {return dispatch(receivePlaces(places))})
                .catch(error => {return dispatch(apiError(searchPlace, error.message))});
        }
    }
}