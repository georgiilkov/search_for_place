import {
    REQUEST_PLACES, RECEIVE_PLACES, API_ERROR, INPUT_ERROR
} from '../actions';

const searchPlaces = (state = {
        searchedPlace: null,
        isFetching: false,
        items: [],
        apiError: null,
        inputError: null
    }, action) => {
    switch (action.type) {
        case REQUEST_PLACES:
            return {
                searchedPlace: action.searchPlace,
                isFetching: true,
                items: [],
                apiError: null,
                inputError: null
            };
        case RECEIVE_PLACES:
            return {
                ...state,
                isFetching: false,
                items: action.places
            };
        case API_ERROR:
            return {
                ...state,
                searchedPlace: action.searchPlace,
                isFetching: false,
                items: [],
                apiError: action.error
            };
        case INPUT_ERROR:
            return {
                ...state,
                items: [],
                inputError: action.error
            };
        default:
            return state;
    }
};

export default searchPlaces;