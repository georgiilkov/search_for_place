import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import '../style/Error.css';

class Error extends React.Component {

    static propTypes = {
        inputError: PropTypes.string,
        apiError: PropTypes.string
    }

    render() {
        if (this.props.inputError) {
            return (<p className="text-danger">{this.props.inputError}</p>);
        } else if (this.props.apiError) {
            return (<p className="apiError alert alert-danger">{this.props.apiError}</p>);
        } else {
            return null;
        }
    }
}

const mapStateToProps = state => {
    const {inputError, apiError} = state;
    return {inputError, apiError};
}

export default connect(mapStateToProps)(Error);