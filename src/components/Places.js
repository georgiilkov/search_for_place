import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';

class Places extends React.Component {

    static propTypes = {
        items: PropTypes.array.isRequired
    }

    render() {
        return(
            <ul className="list-group">
                {this.props.items.map(place => <li className="list-group-item" key={place.id}>{place.name}</li>)}
            </ul>
        )
    }
}

const mapStateToProps = state => {
    const {items} = state;
    return {items};
}

export default connect(mapStateToProps)(Places);