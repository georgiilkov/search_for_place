import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { fetchPlaces } from '../actions';

class SearchForm extends React.Component {

    static propTypes = {
        fetchPlaces: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {input: ""}
    }

    updateInput = input => {
        this.setState({ input });
    }

    handleFormSubmit = (e) => {
        e.preventDefault();
        this.props.fetchPlaces(this.state.input);
        this.setState({ input: "" });
    }

    render() {
        return (
            <form id="placeSearchForm" className="form-inline" onSubmit={e => this.handleFormSubmit(e)}>
                <div className="input-group">
                    <input className="form-control" id="searchPlace" placeholder="Search for..." onChange={e => this.updateInput(e.target.value)}
                           value={this.state.input} type="text" />
                    <span className="input-group-btn">
                     <input className="btn btn-default" type="submit" value="Search" />
                         </span>
                </div>
            </form>
        )
    }
}

export default connect(
    null,
    { fetchPlaces }
)(SearchForm);