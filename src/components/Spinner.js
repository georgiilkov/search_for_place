import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import '../style/Spinner.css';

class Spinner extends React.Component {

    static propTypes = {
        isFetching: PropTypes.bool.isRequired
    }

    render() {
        if (this.props.isFetching) {
            return (<div className="spinner center-block"></div>);
         } else {
             return null;
         }
    }
}

const mapStateToProps = state => {
    const {isFetching} = state;
    return {isFetching};
}

export default connect(mapStateToProps)(Spinner);