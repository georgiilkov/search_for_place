###Main technologies used

--------------

ReactJs, redux


###Prerequisites

--------------

You need to install [NPM](https://nodejs.org/en/ ).


###Installing

--------------

Once you clone or extract the files, navigate to the directory search_for_place via the command prompt and run `npm install`. Start the application using `npm start`. The application should be available at http://localhost:3000 .



###Technical decisions

--------------


The main technologies used for that application are React and Redux. I have used React because it gives us the option to organize the logic of the application into simple components. Using that approach an application can be scaled easily.

Managing the state is probably the most complicated task in the UI and logical mistakes there are often the reason for hard to find defects. Redux gives us the option to manage and control the state in a very simple and elegant way.

For the calls to the API I have used axios. It is insulated in a method getNearbyPlaces. In that way the rest of the application is not dependent on the specific client and it can be easily replaced.

Each React component has its own CSS file. That makes our CSS well organized. Having the JS logic into components and the CSS into small component CSS files simplifies and speeds up the development but has no effect on the app performance since all the CSS and JS gets placed into single JS and CSS files when a production build is created.


The action creators in src\actions\index.js and the reducers in src\reducers\index.js are used for managing the state. The main action creator is fetchPlaces. It gets triggered when the search form is submitted. That function validates the form. If the form is invalid it dispatches an action with type INPUT_ERROR. Dispatching an action is simply sending it to the reducer. The reducer checks the action type and updates the application state accordingly. Using the react-redux method connect we give the React components access to the app state. The Error component displays an error if the state property inputError is true. If the form input is valid, an action with type REQUEST_PLACES is dispatched. In case of such an action the reducer removes the error from the app state, which removes the error from the view, and it sets a flag in the state isFetching. When that flag is set to true, the user can not submit new search requests. That flag is also used in the Spinner component which displays a spinner during the call to the API.

When the API returns a response, an action with type RECEIVE_PLACES and the received places is dispatched. The reducer updates the state with the places. The Places component displays the places.


###Tests

--------------

Sorry, didn't have time to write any.